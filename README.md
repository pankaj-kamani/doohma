# Doohma
## Online advertising platform

#Author
## Doohma Developer


## Dumdata Commands

python manage.py dumpdata user.Businesses --indent=4 >  apps/user/fixtures/business_list.json
python manage.py dumpdata payment.SubscriptionPlans --indent=4 >  apps/payment/fixtures/plans.json
python manage.py loaddata  business_list.json
python manage.py loaddata  plans.json




## google hosting details

user: doohma
pass: doohma
folder: home
env: v_doohma

restart service: sudo supervisorctl restart doohma-app


# google app engine setup details

## postgres detils
connectionName: `doohmauk-dev:europe-west1:doohma-dev-db`
commands : `gcloud auth application-default login`

# process to deploy  django on app engine

## Please visit here for detailed steps
`https://cloud.google.com/python/django/appengine`

# To make bucket publicly avalable 
`gsutil defacl ch -u AllUsers:R gs://yourbucket`
# to make existing objects public
`gsutil acl ch -u AllUsers:R gs://yourbucket/**`