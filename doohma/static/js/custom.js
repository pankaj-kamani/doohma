$(document).ready(function(){
    // $("#join-now").on("click", function(){
    //     $("#modalLoginForm").modal("show");
    //     var email = $(".bannerbutton > input[name='email']").val();
    //     $("#email").val(email);
    // });
    $(document).keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            debugger;
            if ($("#mainlogin").is(":visible")) {
                $("#mainlogin").trigger('click');
            }
            if ($("#ajaxSignup").is(":visible")) {
                $("#ajaxSignup").trigger('click');
            }
            event.preventDefault();
        }
    });
    $('#login').click(function() {
      //Code to be executed when close is clicked
      $('#signupformpopup').trigger('reset');
    });
    $('.close').click(function() {
      //Code to be executed when close is clicked
      $('#signupformpopup').trigger('reset');
    });
    $("#modalsigninForm").on("hidden", function () {
       debugger;
      $('#signupformpopup').trigger('reset');
    });
    $("#signup").click(function(){
        $("body").addClass("modal-opan");
        $('#modalLoginForm').modal('hide');
        $('#modalsigninForm').modal('show');
    });
    $(".poploginbtn >  #mainlogin").on("click", function(){
        $("#signup-data-error").html("");
        $("#login-data-error").html("");
        var username = $.trim($('#loginemail').val());
        var pwd = $.trim($('#password').val());
        $.ajax({
            url : "/account/ajax_login",
            type : "post",
            data : {
                username: username,
                password : pwd,
            }
        }).done(function(data) {
            $("#login-data-error").text(data.message).removeClass('text-danger').addClass('text-success');
            window.location = '/account/business-listing/';
        }).fail(function(data){
            $("#login-data-error").text(JSON.parse(data.responseText).message)
                .addClass('text-danger').removeClass('text-success');
        });
    });

    $(".poploginbtn >  #ajaxSignup").on("click", function(){
        $("#signup-data-error").html("");
        $("#login-data-error").html("");
        var first_name = $.trim($('#first_name').val());
        var last_name = $.trim($('#last_name').val());
        var email = $.trim($('#signupemail').val());
        var mobile = $.trim($('#mobile').val());
        var website = $.trim($('#website').val());
        var facebook_url = $.trim($('#facebook_url').val());
        var password1 = $.trim($('#password1').val());
        var password2 = $.trim($('#password2').val());
        $.ajax({
            url : "/account/signup",
            type : "post",
            data : {
                 first_name: first_name,
                 last_name: last_name,
                 email: email,
                 mobile: mobile,
                 website: website,
                 facebook_url: facebook_url,
                 password1: password1,
                 password2: password2,
            }
        }).done(function(data) {
            $("#signup-data-error").text(data.message).removeClass('text-danger').addClass('text-success');
            $('#signupformpopup').trigger('reset');
        }).fail(function(data){
            var res = JSON.parse(data.responseText);
            if (res.errors){
                $.each(res.errors, function( index, value ) {
                    $("#signup-data-error").append(`<span class='data-error'>${value}<span> <br>`)
                });
            }
            $("#signup-data-error").text(JSON.parse(data.responseText).message).addClass('text-danger').removeClass('text-success')
        });
    });
});

$('.carousel-inner').each(function() {

  if ($(this).children('div').length === 1) $(this).siblings('.controls-top').hide();

});