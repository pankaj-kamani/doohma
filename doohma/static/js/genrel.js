$(document).ready(function () {
    $(window).scroll(function(){
        if ($(window).scrollTop() >= 100) {
            $('nav').addClass('fixed-header');
            $('nav div').addClass('visible-title');
        }
        else {
            $('nav').removeClass('fixed-header');
            $('nav div').removeClass('visible-title');
        }
    });


    $(window).scroll(function() {
    var scrollDistance = $(window).scrollTop();

    // Assign active class to nav links while scolling
    $('.home .page-section').each(function(i) {
        if ($(this).position().top <= scrollDistance) {
                $('.navbar-nav .nav-item.active').removeClass('active');
                $('.navbar-nav .nav-item').eq(i).addClass('active');
        }
        });}).scroll();

    $("[data-dismiss='toast']").click(function () {
        $(".toast").toast('hide')
    });
    $('.home .navbar li').click(function(e) {
        $('.home .navbar li').removeClass('active');
        var $this = $(this);
        if (!$this.hasClass('active')) {
            $this.addClass('active');
        }
        if ($(e.target).parent().prop('id') !== "redirect-button"){
            e.preventDefault();
        }

    });
    $("#signup").click(function(){
        $("#signup-data-error").html("");
        $("#login-data-error").html("");
        $("body").addClass("modal-opan");
        $('#modalLoginForm').modal('hide');
        $('#modalsigninForm').modal('show');
    });
    $(".close").click(function(){
        $("#signup-data-error").html("");
        $("#login-data-error").html("");
        $("body").removeClass("modal-opan");
    });

    //set button id on click to hide first modal

    $("#login").on( "click", function() {
        $("#signup-data-error").html("");
        $("#login-data-error").html("");
        $("body").addClass("modal-opan");
        $('#modalsigninForm').modal('hide');
        $('#modalLoginForm').modal('show');
    });

    $("#btnClosePopup").on( "click", function() {
        $("#signup-data-error").html("");
        $("#login-data-error").html("");
        $("body").removeClass("modal-opan");
        $('#modalLoginForm').modal('hide');
    });
    //trigger next modal
    $(".modal-second-level").on('hidden.bs.modal', function() {
        $("body").addClass("modal-open");
    });

    // file-upload
    $('#chooseFile').bind('change', function () {
      var filename = $("#chooseFile").val();
      if (/^\s*$/.test(filename)) {
        $(".file-upload").removeClass('active');
        $("#noFile").text("No file chosen...");
      }
      else {
        $(".file-upload").addClass('active');
        $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
      }
    });

    // home faq

    var faqMain = document.getElementById("faqs");

    if(faqMain){
        faqMain.addEventListener("click", toggleFaq);
    }

    function toggleFaq(e){ // Open & Close each FAQ function
        if (e.target.tagName == "H2" || e.target.tagName == "h2") return;

        else if (e.target.tagName == "P" || e.target.tagName == "p" || e.target.tagName == "SPAN" || e.target.tagName == "span"){
            e.target.parentNode.parentNode.classList.toggle("active");
        }

        else if (e.target.tagName == "DIV" || e.target.tagName == "div"){
            e.target.parentNode.classList.toggle("active");
            console.log(e.target);
        }
    }


    // init SmoothScroll from library
    var scroll = new SmoothScroll('a[href*="#"]', {
        speed: 660,
        clip: true,
        easing: 'easeInOutQuad'
    });

    //document.querySelecterAll(".faq_sections li");
    function myFunction() {

        $(".broadcastbtn button").on("click", function () {
            $('.broadcastbtn').modal('hide');
        });
    //trigger next modal
        $(".broadcastbtn button").on("click", function () {
            $('.broadtogglebtn').modal('show');
        });
    }


    $(document).ready(function() {
        $('a[href=#top]').click(function(){
            $('html, body').animate({scrollTop:0}, 'slow');
            return false;
        });

    });
        $owlContainer = $('.carousel ');
        $owlSlides    = $owlContainer.children('div');

         // More than one slide - initialize the carousel
        if ($owlSlides.length > 1) {
            $owlContainer.owlCarousel({
             // options go here
            });
         // Only one slide - do something else
        } else {
            $('.carousel').carousel({
                interval: false,
                autoplay: false,
            });
        }
});


$(document).ready(function () {
    //  $('#example').dataTable({
    //     "fnDrawCallback": function(oSettings) {
    //         if($('#example tr').length < 11) {
    //             $('.dataTables_paginate').hide();
    //         }
    //     }
    // });
    // $('a[href*=#]').bind('click', function(e) {
    //     e.preventDefault(); // prevent hard jump, the default behavior
    //     var target = $(this).attr("href"); // Set the target as variable
    //     // perform animated scrolling by getting top-position of target-element and set it as scroll target
    //     $('html, body').stop().animate({
    //             scrollTop: $(target).offset().top
    //     }, 600, function() {
    //             location.hash = target; //attach the hash (#jumptarget) to the pageurl
    //     });
    //     return false;
    // });
});

$('.carousel-inner').each(function() {

          if ($(this).children('div').length === 1) $(this).siblings('.controls-top').hide();

      });