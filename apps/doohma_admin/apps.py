from django.apps import AppConfig


class AdminConfig(AppConfig):
    name = 'doohma_admin'
