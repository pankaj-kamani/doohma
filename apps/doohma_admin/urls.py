
from django.urls import path
from apps.doohma_admin import views

urlpatterns = [
    path('login', views.admin_login, name='admin_login'),
    path('logout', views.logout_view, name='admin_logout'),
    path('users', views.user_list, name='user-list'),
    path('campaigns', views.CampaignView.as_view(), name='campaign-list'),
    path('campaigns/<int:pk>', views.CampaignView.as_view(), name='campaign-details'),
]