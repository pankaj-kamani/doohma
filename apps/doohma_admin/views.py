import logging

from django.conf import settings
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin
from django.core.exceptions import ImproperlyConfigured
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import View

from apps.adverts.constants import RejectOptions
from apps.adverts.models import Advert
from apps.user.forms import LoginForm

User = get_user_model()
logger = logging.getLogger(__name__)


def admin_login(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse('home'))
    if request.method == 'GET':
        form = LoginForm()
        return render(request, 'login.html', {'form': form})
    form = LoginForm(request.POST)
    if form.is_valid():
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            remember = request.POST.get('remember_me')
            if remember:
                settings.SESSION_EXPIRE_AT_BROWSER_CLOSE = False
            return HttpResponseRedirect(reverse('user-list'))
        form.add_error('', 'username or password incorrect')
    return render(request, 'login.html', {'form': form})


def logout_view(request):
    logout(request)
    url = reverse("admin_login")
    return redirect(url, args=(), kwargs={})

def is_admin(user):
    return user.is_superuser


@login_required
@user_passes_test(is_admin, login_url='/doohma_admin/login')
def user_list(request):
    users = User.objects.exclude(is_staff=True, is_superuser = True).select_related('profile').order_by('-id')
    page = request.GET.get('page', 1)
    paginator = Paginator(users, settings.PAGE_SIZE)
    try:
        users = paginator.page(page)
    except PageNotAnInteger:
        users = paginator.page(1)
    except EmptyPage:
        users = paginator.page(paginator.num_pages)
    return render(request, 'user_list.html', locals())


def get_subscription_plan(advert):
    if advert.subscription_set.count():
        return advert.subscription_set.filter(is_active=True).latest("created_ts")
    else:
        return None


class CampaignView(UserPassesTestMixin, LoginRequiredMixin, View):
    template_name = 'campaign-list.html'
    details_template = 'campaign-details.html'
    login_url = '/doohma_admin/login'
    permission_denied_message = 'Admin access level is required to access this view'

    def get_login_url(self):
        """
        Override this method to override the login_url attribute.
        """
        login_url = self.login_url or settings.LOGIN_URL
        if not login_url:
            raise ImproperlyConfigured(
                '{0} is missing the login_url attribute. Define {0}.login_url, settings.LOGIN_URL, or override '
                '{0}.get_login_url().'.format(self.__class__.__name__)
            )

        return str(login_url)

    def test_func(self):
        user = self.request.user
        if not user.is_authenticated and user.is_staff:
            logout(self.request)
        return user.is_authenticated and user.is_staff

    def get(self, request, pk=None):
        reject_options = RejectOptions.get_choices()
        if pk:
            advert = get_object_or_404(Advert, pk=pk)
            subscription = get_subscription_plan(advert)
            medias = advert.image_set.all()
            is_single_approved = medias.filter(is_approved=True).exists()
            all_approved =  medias.filter(is_approved=False, is_rejected=False).count() == 0
            all_reviews_and_sapproved = all_approved and is_single_approved
            return render(request, self.details_template, locals())
        else:
            adverts_list = Advert.objects.prefetch_related('subscription_set').all().order_by('-id')
            page = request.GET.get('page', 1)
            paginator = Paginator(adverts_list, settings.PAGE_SIZE)
            try:
                ads = paginator.page(page)
            except PageNotAnInteger:
                ads = paginator.page(1)
            except EmptyPage:
                ads = paginator.page(paginator.num_pages)
            ad_list = []
            for item in ads:
                sub = get_subscription_plan(item)
                ad_list.append({
                    "ad_id": item.id,
                    "name": item.user.get_username(),
                    "ad_title": item.ad_title,
                    "amount": sub.plan_details.get('cost') if sub else None,
                    "start_date": item.start_date if sub else None,
                    "end_date": item.end_date if sub else None,
                    "status": item.get_status_display()
                })
            return render(request, self.template_name, locals())