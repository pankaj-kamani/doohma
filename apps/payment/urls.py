from django.urls import path
from apps.payment import views as payment_view

urlpatterns = [
    path('plans', payment_view.PlanView.as_view(), name='plan-list'),
    path('plan/create', payment_view.create_plan, name='plan-create'),
    path('plan/<int:pk>/edit', payment_view.manage_plan, name='plan-edit'),
    path('plan/<int:pk>/delete', payment_view.delete_plan, name='plan-delete'),
    path("locations/", payment_view.locations, name="locations"),
    path("pay/", payment_view.locations, name="pay"),
    path("payment-success/", payment_view.payment_success, name="pay-success"),
    path("confirm_pay/<int:package_id>", payment_view.confirm_pay, name="confrim_pay"),
]