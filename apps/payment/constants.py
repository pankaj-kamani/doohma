class VideoQualityTypes:
    HD = 1
    FHD = 2
    UHD = 3
    _4K = 4


    FieldStr = {
        HD : "HD",
        FHD: "FHD",
        UHD: "UHD",
        _4K: "4K"

    }

    @classmethod
    def get_choices(cls):
        return cls.FieldStr.items()


class BooleanTypes:
    YES = 1
    NO = 0

    FieldStr = {
        YES : "Yes",
        NO: "No",

    }

    @classmethod
    def get_choices(cls):
        return cls.FieldStr.items()