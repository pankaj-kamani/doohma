import logging

import stripe
from datetime import datetime, timedelta
from django.conf import settings
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required, user_passes_test, permission_required
from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin
from django.core.exceptions import ImproperlyConfigured, ObjectDoesNotExist
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.forms import model_to_dict
from django.http import HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.urls import reverse
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views import View

from apps.adverts.constants import AdvertStatus
from apps.adverts.models import Advert
from apps.payment.forms import PlanForm
from apps.payment.models import Locations, SubscriptionPlans, Subscription
from apps.payment.permissions import user_passes_admin

stripe.api_key = settings.STRIPE_SECRET_KEY # new

logger = logging.getLogger('api')

REDIRECT_FIELD_NAME = 'next'

def is_admin_user(user):
    if not (user.is_authenticated and user.is_staff):
        return False
    return user.is_authenticated and user.is_staff

def admin_required(function=None, redirect_field_name=REDIRECT_FIELD_NAME, login_url=None):
    """
    Decorator for views that checks that the user is logged in, redirecting
    to the log-in page if necessary.
    """
    actual_decorator = user_passes_test(
        lambda u: u.is_authenticated,
        login_url=login_url,
        redirect_field_name=redirect_field_name
    )
    if function:
        return actual_decorator(function)
    return actual_decorator


@login_required
def locations(request):
    locations = Locations.objects.all()
    page = request.GET.get('page', 1)
    paginator = Paginator(locations, settings.PAGE_SIZE)
    try:
        locs = paginator.page(page)
    except PageNotAnInteger:
        locs = paginator.page(1)
    except EmptyPage:
        locs = paginator.page(paginator.num_pages)
    ad_to_pay = request.session['ad_to_pay']
    if ad_to_pay:
        request.session['ad_step'] = {"ad_id": ad_to_pay, "next_step": "plans"}

    return render(request, 'locations.html', locals())

@login_required
def subscription_plans(request, pk):
    try:
        ad_edit = request.session.get('ad_edit')
        if ad_edit.get('ad_id') == pk and ad_edit.get('edit'):
            request.session['ad_edit'] = {"ad_id": pk, 'edit': False}
            return HttpResponseRedirect(reverse('adverts_list'))
    except:
        logger.info("By passing the redirect as ad is created")
    loc_id = request.GET.get('loc_id', None)
    try:
       sel_loc = Locations.objects.get(id=int(loc_id))
    except:
        messages.add_message(request, messages.ERROR, 'No Such location exists')
        return redirect("locations")
    request.session['loc_selected'] =  sel_loc.id
    request.session['ad_id'] = pk
    request.session['ad_step'] = {"ad_id": pk, 'loc_selected':sel_loc.id,  "next_step": "sub-plans"}
    plans = SubscriptionPlans.objects.all().order_by('id')
    key = settings.STRIPE_PUBLISHABLE_KEY
    return render(request, 'sub_plans.html', locals())


@login_required
def confirm_pay(request, package_id):
    sub_plan = get_object_or_404(SubscriptionPlans, pk=package_id)
    location = get_object_or_404(Locations, pk=request.session['loc_selected'])
    advert = get_object_or_404(Advert, pk=request.session['ad_id'])
    request.session['ad_step'] = {"package_id": package_id, "ad_id": request.session['ad_id'], "next_step": "confrim_pay"}

    if request.method == "GET":
        if advert.is_paid:
            plan = advert.subscription_set.latest('created_ts')
            if plan.end_date > timezone.now():
                if plan.package.id == package_id:
                    return HttpResponseRedirect(reverse('adverts_list'))
        try:
            key = settings.STRIPE_PUBLISHABLE_KEY
            # count validity if there is else count 30 days
            validity = sub_plan.validity if sub_plan.validity else 30
            end_date = datetime.now()+ timedelta(days=validity)
            try:
                if advert.start_date:
                    end_date = advert.start_date + timedelta(days=validity)
            except:
                pass
            return render(request, 'confirm_and_pay.html', locals())
        except:
            messages.add_message(request, messages.ERROR, 'Payment Compromized')
            return HttpResponseRedirect(reverse('subscribe', kwargs={'pk': 1}))
    else:
        try:
            sub_plan = get_object_or_404(SubscriptionPlans, pk=package_id)
            validity = sub_plan.validity if sub_plan.validity else 30
            charge = stripe.Charge.create(
                amount=round(sub_plan.cost*100),
                currency="gbp",
                description=sub_plan.title,
                source=request.POST['stripeToken']
            )
            if charge:
                details = charge.to_dict()
                advert.subscription_set.filter(is_active=True).update(is_active=False)
                auto_renew =  request.POST.get('auto_renew')
                sub = Subscription.objects.create(
                    package=sub_plan,
                    advert=advert,
                    gateway_response=charge.to_dict(),
                    plan_details={
                        "title": sub_plan.title,
                        "screens": sub_plan.screens,
                        "duration": 0,
                        "per_month_quota": sub_plan.per_month_quota,
                        "is_analytics": sub_plan.is_analytics,
                        "validity": sub_plan.validity,
                        "cost":sub_plan.cost,
                        "is_recurrent": True if auto_renew == "on" else False,
                        "location": [item.id for item in sub_plan.location.all()]
                    },
                    is_recurrent =True if auto_renew == "on" else False,
                    created_by=request.user,
                    updated_by=request.user,
                    is_active=True,
                    start_date=datetime.now(),
                    end_date=datetime.now() + timedelta(days=validity),
                    renewal_date=datetime.now()+ timedelta(days=validity-5))
                advert.is_paid = True
                advert.end_date = None if auto_renew == "on" else (datetime.now() + timedelta(days=validity)).strftime("%Y-%m-%d")
                advert.status = AdvertStatus.Requested
                advert.save()
                logger.info("deleting ad key from session")
                # messages.add_message(request, messages.SUCCESS, 'Payment completed Successfully')
                return HttpResponseRedirect(reverse('pay-success'))
        except Exception as e:
            logger.critical("Exception caught", exc_info=True)
            messages.add_message(request, messages.ERROR, str(e))
            return HttpResponseRedirect(reverse('confrim_pay', kwargs={"package_id": package_id}))

@login_required
def payment_success(request):
    return render(request, 'thank-you.html', locals())


class PlanView(UserPassesTestMixin, LoginRequiredMixin, View):

    create_template = 'create-plan.html'
    list_template = 'plan-list.html'
    model_form = PlanForm
    login_url = '/doohma_admin/login'
    permission_denied_message = 'Admin access level is required to access this view'

    def get_login_url(self):
        """
        Override this method to override the login_url attribute.
        """
        login_url = self.login_url or settings.LOGIN_URL
        if not login_url:
            raise ImproperlyConfigured(
                '{0} is missing the login_url attribute. Define {0}.login_url, settings.LOGIN_URL, or override '
                '{0}.get_login_url().'.format(self.__class__.__name__)
            )

        return str(login_url)

    def test_func(self):
        user = self.request.user
        if not user.is_authenticated and user.is_staff:
            logout(self.request)
        return user.is_authenticated and user.is_staff

    def get(self, request, pk=None):
        plans = SubscriptionPlans.objects.all().order_by('-id')
        page = request.GET.get('page', 1)
        paginator = Paginator(plans, settings.PAGE_SIZE)
        try:
            plans = paginator.page(page)
        except PageNotAnInteger:
            plans = paginator.page(1)
        except EmptyPage:
            plans = paginator.page(paginator.num_pages)
        return render(request, self.list_template, locals())

@login_required
@user_passes_admin(is_admin_user)
def manage_plan(request, pk):
    if not request.user.is_superuser:
        return HttpResponseForbidden()
    if request.method == 'GET':
        plan = get_object_or_404(SubscriptionPlans, pk=pk)
        form = PlanForm(instance=plan)
        return render(request, 'create-plan.html', {"form": form})

    if request.method == 'POST':
        plan =  get_object_or_404(SubscriptionPlans, pk=pk)
        form = PlanForm(request.POST, instance=plan)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('plan-list'))
        return render(request, 'create-plan.html', {'form': form})

    if request.method == 'DELETE':
        plan = get_object_or_404(SubscriptionPlans, pk=pk)
        plan.delete()
        return reverse('plan-list')


@login_required
@user_passes_admin(is_admin_user)
def create_plan(request):
    if request.method == "POST":
        form = PlanForm(request.POST)
        if form.is_valid():
            plan = form.save()
            logger.info("Saving New plan {}".format(plan.title))
            return HttpResponseRedirect(reverse('plan-list'))
        else:
            return render(request, 'create-plan.html', {"form": form})
    else:
        form = PlanForm()
        return render(request, 'create-plan.html', {"form": form})

@login_required
@user_passes_admin(is_admin_user)
def delete_plan(request, pk):
    if request.method == 'GET':
        if request.user.is_superuser:
            plan = get_object_or_404(SubscriptionPlans, pk=pk)
            plan.delete()
            #messages.add_message(request,messages.INFO, "Plan %s has been deleted successfully" % plan.title)
    #messages.add_message(request, messages.ERROR, "Plan Delete is not allowed")
    return HttpResponseRedirect(reverse('plan-list'))
