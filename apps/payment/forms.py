from django import forms

from apps.payment.constants import VideoQualityTypes, BooleanTypes
from apps.payment.models import SubscriptionPlans


class LocForm(forms.Form):
    loc_id = forms.IntegerField(required=True)


class PlanForm(forms.ModelForm):

    video_quality = forms.ChoiceField(widget=forms.RadioSelect(),
        choices=VideoQualityTypes.get_choices(),

    )
    allow_static_ad_design = forms.ChoiceField(
        choices=BooleanTypes.get_choices(),
        widget=forms.RadioSelect()
    )
    allow_video_ad_design = forms.ChoiceField(
        choices=BooleanTypes.get_choices(),
        widget=forms.RadioSelect()
    )
    is_app_allowed = forms.ChoiceField(
        choices=BooleanTypes.get_choices(),
        widget=forms.RadioSelect()
    )

    class Meta:
        model = SubscriptionPlans
        fields = (
            "title",
            "screens",
            "duration",
            "per_month_quota",
            "changes_allowed",
            "is_analytics",
            "is_app_allowed",
            "cost",
            "video_quality",
            "allow_static_ad_design",
            "allow_static_ad_design_count",
            "allow_video_ad_design_count",
            "allow_video_ad_design",
            "broadcast_allowance",
        )

    def clean(self):
        cleaned_data = super(PlanForm, self).clean()
        if cleaned_data.get('allow_static_ad_design') == '1':
            if not cleaned_data.get('allow_static_ad_design_count'):
                self.add_error('allow_static_ad_design_count', "Static ad design Count Required")
        if cleaned_data.get('allow_video_ad_design') == '1':
            if not cleaned_data.get('allow_video_ad_design_count'):
                self.add_error('allow_video_ad_design_count', "Video ad design Count Required")
        return cleaned_data
