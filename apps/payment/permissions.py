from functools import wraps

from django.conf import settings
from django.contrib.auth.views import redirect_to_login
from django.http import HttpResponseRedirect
from django.shortcuts import resolve_url


def user_passes_admin(test_func, not_found_url=None):
    """
    Decorator for views that checks that the user passes the given test,
    redirecting to the log-in page if necessary. The test should be a callable
    that takes the user object and returns True if the user passes.
    """

    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            if test_func(request.user):
                return view_func(request, *args, **kwargs)
            resolved_404_url = resolve_url(not_found_url or settings.NOT_FOUND_URL)
            # If the login url is the same scheme and net location then just
            # use the path as the "next" url.
            return HttpResponseRedirect(resolved_404_url)
        return _wrapped_view
    return decorator