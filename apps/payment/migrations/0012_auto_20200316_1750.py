# Generated by Django 2.0 on 2020-03-16 17:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payment', '0011_auto_20200316_1735'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='subscriptionplans',
            name='allow_static_ad_design'
        ),
        migrations.RemoveField(
            model_name='subscriptionplans',
            name='allow_video_ad_design'
        ),

        migrations.RemoveField(
            model_name='subscriptionplans',
            name='is_app_allowed'
        ),

        migrations.AddField(
            model_name='subscriptionplans',
            name='allow_static_ad_design',
            field=models.SmallIntegerField(choices=[(1, 'Yes'), (0, 'No')], default=0),
        ),
        migrations.AddField(
            model_name='subscriptionplans',
            name='allow_video_ad_design',
            field=models.SmallIntegerField(choices=[(1, 'Yes'), (0, 'No')], default=0),
        ),
        migrations.AddField(
            model_name='subscriptionplans',
            name='is_app_allowed',
            field=models.SmallIntegerField(choices=[(1, 'Yes'), (0, 'No')], default=0),
        ),
    ]
