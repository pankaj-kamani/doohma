# Generated by Django 2.0 on 2020-01-04 05:36

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('payment', '0005_auto_20200102_1611'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subscription',
            name='advert',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='subscription_set', to='adverts.Advert'),
        ),
    ]
