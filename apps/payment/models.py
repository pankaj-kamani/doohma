import os
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres.fields import JSONField
from django.db import models
from apps.adverts.models import Advert
from apps.common.base_model import BaseModel
from apps.payment.constants import VideoQualityTypes, BooleanTypes


class Locations(BaseModel):
    name = models.CharField(max_length=255)
    lat = models.DecimalField(max_digits=11, decimal_places=9)
    lng = models.DecimalField(max_digits=11, decimal_places=9)
    image = models.ImageField(upload_to='locations/', blank=True, null=True)
    address = models.TextField()
    google_link = models.URLField(blank=True)
    footfall = models.CharField(max_length=255, blank=True)


    class Meta:
        db_table = "dh_locations"
        verbose_name_plural = "Locations"

    @property
    def get_image(self):
        """
        returns the url of the user avatar
        :return:
        """
        try:
            return self.image.url
        except ValueError:
            return None

class SubscriptionPlans(BaseModel):
    title = models.CharField(max_length=50)
    screens = models.PositiveIntegerField()
    location = models.ManyToManyField(Locations, related_name='locations')
    duration = models.DurationField(blank=True, null=True)
    per_month_quota = models.PositiveIntegerField(blank=True, null=True)
    changes_allowed = models.PositiveIntegerField(blank=True, null=True)
    is_analytics = models.BooleanField(default=False)
    is_app_allowed = models.PositiveSmallIntegerField(choices=BooleanTypes.get_choices(), default=BooleanTypes.NO)
    validity = models.PositiveIntegerField("Validity(Days)", blank=True, null=True)
    cost = models.FloatField(default=0.0)
    video_quality = models.PositiveSmallIntegerField(choices=VideoQualityTypes.get_choices(), default=VideoQualityTypes.HD)
    allow_static_ad_design = models.PositiveSmallIntegerField(choices=BooleanTypes.get_choices(), default=BooleanTypes.NO)
    allow_static_ad_design_count = models.PositiveIntegerField(default=0, blank=True, null=True)
    allow_video_ad_design_count = models.PositiveIntegerField(default=0, blank=True, null=True)
    allow_video_ad_design = models.PositiveSmallIntegerField(choices=BooleanTypes.get_choices(), default=BooleanTypes.NO)
    broadcast_allowance = models.CharField(max_length=255, blank=True, null=True)


    def __str__(self):
        return "{}".format(self.title)

    class Meta:
        db_table = "dh_subscriptions_plans"
        verbose_name_plural = "Subscriptions Plans"


class Subscription(BaseModel):
    package = models.ForeignKey(SubscriptionPlans, on_delete=models.SET_NULL, null=True, blank=True)
    advert = models.ForeignKey(Advert, on_delete=models.SET_NULL, null=True, related_name='subscription_set')
    start_date = models.DateTimeField(auto_now_add=True)
    end_date = models.DateTimeField(blank=True, null=True)
    renewal_date = models.DateTimeField(blank=True, null=True)
    gateway_response = JSONField(blank=True, null=True)
    plan_details = JSONField("Plan Allocated")
    is_active = models.BooleanField(default=True)
    is_recurrent = models.BooleanField(_("Auto Debit"), default=False)


    class Meta:
        db_table = "dh_subscriptions"
        verbose_name_plural = "Subscriptions"