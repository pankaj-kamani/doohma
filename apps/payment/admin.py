from django.contrib import admin

# Register your models here.
from apps.common.base_admin import BaseAdmin
from apps.payment.models import *


class LocationsAdmin(BaseAdmin):
    list_display = ['name', 'lat', 'lng']


class SubscriptionPlansAdmin(BaseAdmin):
    list_display = [
        "title",
        "screens",
        "duration",
        "per_month_quota",
        "changes_allowed",
        "is_analytics",
        "is_app_allowed",
        "cost",
    ]


class SubscriptionAdmin(BaseAdmin):
    list_display = [
       "package",
       "advert",
       "start_date",
       "end_date",
       "renewal_date"
    ]


admin.site.register(Subscription, SubscriptionAdmin)
admin.site.register(SubscriptionPlans, SubscriptionPlansAdmin)
admin.site.register(Locations, LocationsAdmin)