from apps.common.constant import GlobalConstant


class UserTypes(GlobalConstant):
    Normal = 1
    Staff = 2
    Admin = 3

    FieldStr = {
        Normal: "Normal",
        Staff: "Staff",
        Admin: "Admin",
    }
    @classmethod
    def get_choices(cls):
        return cls.FieldStr.items()