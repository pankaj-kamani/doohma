from django.conf.urls import url
from django.urls import path
from apps.user import views


urlpatterns = [
    path('login', views.user_login, name='login'),
    path('ajax_login', views.ajax_login, name='ajax-login'),
    path('logout', views.logout_view, name='logout'),
    path('signup', views.signup_view, name='signup'),
    path('activate', views.activate_user, name='activate_user'),
    url(r'^settings/$', views.settings, name='settings'),
    url(r'^settings/password/$', views.password, name='password'),
    path('wel-come/', views.user_landing, name='user_landing'),
    path('business-listing/', views.business_listing, name='business_listing'),
]