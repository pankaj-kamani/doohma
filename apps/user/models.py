import uuid

from django.conf import settings
from django.contrib.auth.base_user import AbstractBaseUser
from django.core.mail import send_mail
from django.db import models
from django.contrib.auth.models import AbstractUser, PermissionsMixin
from django.utils.translation import ugettext_lazy as _

from apps.common.base_model import BaseModel
from apps.user.constants import UserTypes
from apps.user.managers import UserManager
from autoslug import AutoSlugField

def get_image_path(instance, filename):
    """
    method to create separate folder for each user
    :param instance:
    :param filename:
    :return: file_path
    """
    return 'user_{0}/{1}'.format(instance.id, filename)


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_('email address'), unique=True)
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    date_joined = models.DateTimeField(_('date joined'), auto_now_add=True)
    is_active = models.BooleanField(_('active'), default=True)
    is_staff = models.BooleanField(_('staff status'), default=False)
    avatar = models.ImageField(upload_to='avatars/', null=True, blank=True)
    key = models.CharField(max_length=255, blank=True, null=True)
    user_type= models.IntegerField(choices=UserTypes.get_choices(), default=UserTypes.Normal)
    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['user_type']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    @property
    def get_avatar(self):
        """
        returns the url of the user avatar
        :return:
        """
        try:
            return self.avatar.url
        except ValueError:
            return None

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """
        Returns the short name for the user.
        """
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)


class EmailVerificationTokens(models.Model):
    key = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = 'eb_email_tokens'
        verbose_name_plural = 'Email verification Tokens'

    def __str__(self):
        return str(self.user)


class Businesses(BaseModel):
    title = models.CharField(max_length=255)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.CASCADE)
    slug = AutoSlugField(populate_from='title')

    def __str__(self):
        return "{}".format(self.title)

    class Meta:
        db_table = 'dh_business'
        verbose_name_plural = 'Business List'


class UserProfile(BaseModel):
    user = models.OneToOneField(User, related_name='profile', on_delete=models.CASCADE)
    profession = models.ForeignKey(Businesses, related_name='profession', on_delete=models.SET_NULL, blank=True, null=True)
    mobile = models.CharField(max_length=13, blank=True, null=True)
    facebook_url = models.URLField(blank=True, null=True)
    website = models.URLField(blank=True, null=True)


    def __str__(self):
        return "{}{}".format(self.user.email, self.profession)

    class Meta:
        db_table = 'dh_user_Profile'
        verbose_name_plural = 'Profiles'