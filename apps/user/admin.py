from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from django.utils.translation import ugettext_lazy as _

from apps.common.base_admin import BaseAdmin
from apps.user.models import Businesses, UserProfile

User = get_user_model()


class ProfileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False
    fk_name = "user"
    readonly_fields = ['created_by', 'updated_by']

class UserAdmin(BaseUserAdmin):
    fieldsets = (
        (None, {'fields': ('email', 'password', 'key', 'avatar')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name')}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        }),
        (_('Important dates'), {'fields': ('last_login',)}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    list_display = ('email', 'first_name', 'last_name', 'is_staff')
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups')
    search_fields = ('first_name', 'last_name', 'email')
    ordering = ('id',)
    readonly_fields = ('date_joined','email')
    filter_horizontal = ('groups', 'user_permissions',)
    inlines = (ProfileInline,)


class BusinessesAdmin(BaseAdmin):
    list_display = ['title', 'slug']

admin.site.register(User, UserAdmin)
admin.site.register(Businesses, BusinessesAdmin)
