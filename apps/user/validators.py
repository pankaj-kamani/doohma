from django.contrib.auth.password_validation import NumericPasswordValidator, MinimumLengthValidator
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _, ngettext


class NumericPasswordValidatorCustom(NumericPasswordValidator):
    """
        Validate whether the password is alphanumeric.
    """

    def validate(self, password, user=None):
        if password.isdigit():
            raise ValidationError(
                _("Password should contain at least 1 character, 1 special character and 1 number"),
                code='password_entirely_numeric',
            )

    def get_help_text(self):
        return _("Your password can't be entirely numeric.")


class MinimumLengthValidatorCustom(MinimumLengthValidator):
    """
    Validate whether the password is of a minimum length.
    """
    def validate(self, password, user=None):
        if len(password) < self.min_length:
            raise ValidationError(
                ngettext(
                    "Enter 8 character strong password",
                    "Enter 8 character strong password",
                    self.min_length
                ),
                code='password_too_short',
                params={'min_length': self.min_length},
            )

    def get_help_text(self):
        return ngettext(
            "Your password must contain at least %(min_length)d character.",
            "Your password must contain at least %(min_length)d characters.",
            self.min_length
        ) % {'min_length': self.min_length}