import re

from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm, UsernameField
from django import forms
from django.core.validators import MinLengthValidator, MaxLengthValidator
from .validators import  MinimumLengthValidatorCustom, NumericPasswordValidatorCustom
from apps.user.models import Businesses, UserProfile

User = get_user_model()

min_validator = MinLengthValidator(10)
max_validator = MaxLengthValidator(10)
min_validator.message = 'Enter Valid Mobile Number'
max_validator.message = 'Enter Valid Mobile Number'

# various validators for custom password strings

class SignUpForm(UserCreationForm):

    error_messages = {
        'password_mismatch': "Password and Confirm Password does not match",
    }

    first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    email = forms.EmailField(
        max_length=254,
        error_messages={
            'required': "Enter Valid Email Address"
        },
        help_text='Required. Inform a valid email address.'
    )
    mobile = forms.CharField(
        error_messages={
            'mobile': "Enter Valid Mobile Number"
        },
        validators=[min_validator, max_validator],
        required=False)
    facebook_url = forms.URLField(required=False)
    website = forms.URLField(required=False)
    password2 = forms.CharField(error_messages={
        'required': "Confirm password is required"
    })
    password1 = forms.CharField(error_messages={
        'required': "Password is required"
    })
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'mobile', 'facebook_url', 'website', 'password1', 'password2')
        field_classes = {'username': UsernameField}

    def save(self, commit=True):
        sf = super(SignUpForm, self).save(commit=False)
        if commit:
            sf.save()
            profile, created = UserProfile.objects.get_or_create(user=sf)
            profile.facebook_url = self.cleaned_data.get('facebook_url')
            profile.mobile =  self.cleaned_data.get('mobile')
            profile.website = self.cleaned_data.get('website')
            profile.save()
        return sf


class LoginForm(forms.Form):
    username = forms.EmailField(required=True)
    password = forms.CharField(required=True)


class ProfileEditForm(forms.ModelForm):
    daily_mail = forms.BooleanField(required=False)
    news = forms.BooleanField(required=False)
    subscriptions = forms.BooleanField(required=False)
    avatar = forms.ImageField(widget=forms.widgets.FileInput)

    class Meta:
        model = User
        fields = ['avatar', 'first_name', 'last_name', 'daily_mail', 'news', 'subscriptions']


class BusinessForm(forms.Form):
    custom_business = forms.CharField(required=False)
    title = forms.CharField(required=False)

    def clean(self):
        cleaned_data = super().clean()
        title = cleaned_data.get("title", None)
        custom_business = cleaned_data.get("custom_business", None)
        if not (title or custom_business):
            self.add_error("title", "One Input Required")