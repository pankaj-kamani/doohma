import logging

from django.conf import settings
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse
from django.utils.crypto import get_random_string
from django.views.decorators.csrf import csrf_exempt

from apps.common.mailer.email import send_email
from apps.user.forms import SignUpForm, LoginForm, ProfileEditForm, BusinessForm
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AdminPasswordChangeForm, PasswordChangeForm
from django.contrib.auth import update_session_auth_hash
from django.contrib import messages
from django.shortcuts import render, redirect

from social_django.models import UserSocialAuth


from apps.user.functions import get_user_context
from apps.user.models import Businesses, UserProfile

User = get_user_model()

logger = logging.getLogger('api')


def logout_view(request):
    logout(request)
    url = reverse("home")
    return redirect(url, args=(), kwargs={})


def user_login(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse('home'))
    if request.method == 'GET':
        form = LoginForm()
        return render(request, './login.html', {'form': form})
    form = LoginForm(request.POST)
    if form.is_valid():
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            remember = request.POST.get('remember_me')
            if remember:
                settings.SESSION_EXPIRE_AT_BROWSER_CLOSE = False
            return HttpResponseRedirect(reverse('home'))
        form.add_error('', 'username or password incorrect')
    return render(request, 'login.html', {'form': form})


@csrf_exempt
def ajax_login(request):
    username= request.POST.get('username')
    password = request.POST.get('password')
    request.session.set_expiry(0)
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            return JsonResponse({'message': ""}, status=200)
        else:
            return JsonResponse({'message': "User is not active"}, status=400)
    else:
        return JsonResponse({'message': "Email or Password is incorrect"}, status=400)



def activate_user(request):
    if request.method=="GET":
        key = request.GET.get('key')
        email = request.GET.get('email')
        user = request.GET.get('user')
        try:
            user = User.objects.get(email=email, key=key)
            user.is_active=True
            user.save()
            login(request, user, backend='django.contrib.auth.backends.ModelBackend')
            return redirect('business_listing')
        except ObjectDoesNotExist:
            return HttpResponse("Error in email activation please try again")


@csrf_exempt
def signup_view(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user_instance = form.save()
            string = get_random_string(100)
            while User.objects.filter(key=string):
                string = get_random_string(length=25)
            user_instance.key = string
            user_instance.is_active = False
            user_instance.save()
            logger.info("Sending mail for account verification to email {}".format(user_instance.email))
            res = send_email(request, user_instance, [user_instance.email])
            logger.info("Response From mail client {}".format(res))
            return JsonResponse({"message": "Please Confirm your email to continue"}, status=200)
        else:
            return JsonResponse({"errors": form.errors}, status=400)
    else:
        return JsonResponse({"message": "Method `GET` not Allowed"}, status=400)


@login_required
def index(request):
    context = get_user_context(request)
    return render(request, 'index.html', context.flatten())


@login_required
def manage_profile(request):
    if request.method == 'POST':
        profile_form = ProfileEditForm(request.POST, request.FILES, instance=request.user)
        if profile_form.is_valid():
            profile_form.save()
            return HttpResponseRedirect(reverse('user_profile'))
        else:
            return render(request, 'user_profile.html', {"form": profile_form})
    if request.method == 'GET':
        if request.GET.get('edit') == 'true':
            profile_form = ProfileEditForm(instance=request.user)
            return render(request, 'user_profile.html', {"user": request.user, "form": profile_form, 'edit': True})
        else:
            profile_form = ProfileEditForm(instance=request.user)
            return render(request, 'user_profile.html', {"form": profile_form})


@login_required
def change_api_key(request):
    try:
        user = request.user
        string = get_random_string(100)
        while User.objects.filter(key=string):
            string = get_random_string(length=25)
        user.key = string
        user.save()
        return HttpResponseRedirect(reverse('user_profile'))
    except Exception as e:
        messages.add_message(request, messages.WARNING, 'Batch Does not exists', extra_tags="alert-error")
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


@login_required
def change_notification_preference(request):
    if request.method == 'POST':
        profile_form = ProfileEditForm(request.POST, request.FILES, instance=request.user)
        if profile_form.is_valid():
            profile_form.save()
            return HttpResponseRedirect(reverse('user_profile'))
        else:
            return render(request, 'user_profile.html', {"form": profile_form})
    if request.method == 'GET':
        if request.GET.get('edit') == 'true':
            profile_form = ProfileEditForm(instance=request.user)
            return render(request, 'user_profile.html', {"user": request.user, "form": profile_form, 'edit': True})
        else:
            profile_form = ProfileEditForm(instance=request.user)
            return render(request, 'user_profile.html', {"form": profile_form})



@login_required
def business_listing(request):
    user = request.user
    business_list = Businesses.objects.all
    if request.method == "GET":
        if user.profile.profession:
            return redirect('adverts_list')
        return render(request, 'user_base.html', locals())
    else:
        try:
            form = BusinessForm(request.POST)
            if form.is_valid():
                title = request.POST.get('title')
                custom_business = request.POST.get('custom_business')
                if title:
                    instance, created = Businesses.objects.get_or_create(slug=title)
                elif custom_business:
                    instance, created = Businesses.objects.get_or_create(title=custom_business)
                user.profile.profession = instance
                user.profile.save()
                return redirect('user_landing')
            else:
                form = BusinessForm(request.POST)
                return render(request, 'user_base.html', locals())
        except Exception as e:
            logger.critical("Exception Caught in file {}".format(__file__), exc_info=True)
            return render(request, '500.html', locals())


@login_required
def user_landing(request):
    user = request.user
    return render(request, 'user_welcome.html', locals())


@login_required
def settings(request):
    user = request.user

    try:
        github_login = user.social_auth.get(provider='github')
    except UserSocialAuth.DoesNotExist:
        github_login = None

    try:
        twitter_login = user.social_auth.get(provider='twitter')
    except UserSocialAuth.DoesNotExist:
        twitter_login = None

    try:
        facebook_login = user.social_auth.get(provider='facebook')
    except UserSocialAuth.DoesNotExist:
        facebook_login = None

    try:
        google_login = user.social_auth.get(provider='google-oauth2')
    except UserSocialAuth.DoesNotExist:
        google_login = None

    can_disconnect = (user.social_auth.count() > 1 or user.has_usable_password())

    return render(request, 'settings.html', {
        'github_login': github_login,
        'twitter_login': twitter_login,
        'facebook_login': facebook_login,
        'google_login': google_login,
        'can_disconnect': can_disconnect
    })

@login_required
def password(request):
    if request.user.has_usable_password():
        PasswordForm = PasswordChangeForm
    else:
        PasswordForm = AdminPasswordChangeForm

    if request.method == 'POST':
        form = PasswordForm(request.user, request.POST)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            messages.success(request, 'Your password was successfully updated!')
            return redirect('password')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordForm(request.user)
    return render(request, 'set_password.html', {'form': form})
