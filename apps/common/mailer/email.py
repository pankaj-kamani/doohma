import logging
from django.conf import settings
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from django.template.loader import get_template

logger = logging.getLogger('api')
sg = SendGridAPIClient(settings.SENDGRID_API_KEY)
import ssl
try:
    _create_unverified_https_context = ssl._create_unverified_context
except AttributeError:
    # Legacy Python that doesn't verify HTTPS certificates by default
    pass
else:
    # Handle target environment that doesn't support HTTPS verification
    ssl._create_default_https_context = _create_unverified_https_context

def get_email_verification_context(request, user):
    user_key = user.key
    username = user.get_username()
    email = user.email
    user_id = user.id
    activate_url = "http://{}/account/activate?email={}&key={}&user={}".format(request.get_host(), email, user_key,
                                                                               user_id)
    content = get_template('verify_email1.html').render(
        {
            'username': username,
            'activate_url': activate_url,
        })
    context = {
        "subject": "Email verification at Doohma",
        "html_message": content,
        "from": "no-reply@doohma.com",
    }
    return context


def send_email(request, user, to_addresses):
    context = get_email_verification_context(request, user)
    message = Mail(
        from_email=context.get('from'),
        to_emails=to_addresses,
        subject=context.get('subject'),
        html_content=context.get('html_message'))
    try:
        response = sg.send(message)
        return response.status_code
    except Exception as e:
        logger.info("Exception caught in Sending email {}".format(__file__), exc_info=True)
