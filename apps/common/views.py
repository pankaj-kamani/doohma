import logging

from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect


# Create your views here.
from django.urls import reverse

from apps.user.functions import get_user_context

logger = logging.getLogger('api')

def index(request):
    """
        Main view for loading a homepage and  the landing page on website
    """
    if not request.user.is_anonymous:
        if not request.user.profile.profession:
            return redirect('business_listing')
    return render(request, 'index.html', context=locals())


def privacy_policy(request):
    return render(request, 'privacy_policy.html')

def render_404(request):
    context  = get_user_context(request)
    return render(request, '404.html',locals())
