import logging

import urllib
from django.http import HttpResponseRedirect
from django.urls import reverse
from apps.user.models import UserProfile

logger = logging.getLogger('api')

def create_profile(strategy, details, backend, user=None, *args, **kwargs):
    # Get the logged in user (if any)
    logged_in_user = strategy.storage.user.get_username(user)
    logger.info("creating profile for user: %", user)
    profile, created = UserProfile.objects.get_or_create(user=user)
    logger.info("Profile created for user %s with id %s", user, profile.id)
    return {
        'profile': profile,
    }


from django.contrib.auth import logout

def social_user(backend, uid, user=None, *args, **kwargs):
    provider = backend.name
    social = backend.strategy.storage.user.get_social_auth(provider, uid)
    if social:
        if user and social.user != user:
            logout(backend.strategy.request)
        elif not user:
            user = social.user
    return {'social': social,
            'user': user,
            'is_new': user is None,
            'new_association': False}