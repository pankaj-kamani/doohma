from django.urls import path
from apps.common import views


urlpatterns = [
    path('', views.index, name='home'),
    path('policy', views.privacy_policy, name='policy'),
]
