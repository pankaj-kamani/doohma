import logging
from datetime import datetime, timedelta

from django.conf import settings
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from apps.api import HandleBackend
from apps.api.HandleBackend import GApi
from apps.api.crypt import encrypt_message
from apps.api.decorators import is_token_valid
from apps.api.models import Tokens

logger = logging.getLogger('main')


@csrf_exempt
def api_login(request):
    try:
        data = request.POST
        username = data.get('username', None)
        logger.info("[ API-LOGIN ] User request for api login with username %s " % username)
        password = data.get('password', None)
        g_api = GApi()
        isLogInFailed = g_api.validateUser(username, password)
        if not isLogInFailed:
            # delete all tokens for security reasons
            Tokens.objects.filter(username=username).delete()
            # generate new token based on the username
            token = encrypt_message(username)
            # add expiry of the token
            expiry = datetime.now() + timedelta(days=settings.TOKEN_LIFETIME)
            # save to database for varification of users in subsequent request
            Tokens.objects.create(username=username, token=token, expiry_time=expiry)
            return JsonResponse({
                "status": 200,
                "msg": "Success.",
                "data": {
                    "access_token": token,
                    "isLogInFailed": isLogInFailed,
                }
            }, status=200)
        else:
            token = None
            return JsonResponse({
                "status": 400,
                "msg": "Login Failed.",
                "data": {
                    "access_token": token,
                    "isLogInFailed": isLogInFailed,
                }
            }, status=400)
    except Exception as e:
        return JsonResponse({
            "status": 400,
            "msg": "failed.",
            "data": {
                "error": str(e),
            }}, status=400)


@csrf_exempt
@is_token_valid
def is_online(request):
    try:
        if request.POST.get('isOnline', None) == '1':
            isOnline = True
        else:
            isOnline = False
        g_api = GApi()
        errStatus = g_api.updateScreenStatus(request.username, isOnline)
        return JsonResponse({
            "status": 200,
            "msg": "Success",
            "data": {
                "errStatus": errStatus,
            }
        }, status=200)
    except Exception as e:
        return JsonResponse({
            "status": 400,
            "msg": "failed.",
            "data": {
                "error": str(e),
            }}, status=400)


@csrf_exempt
@is_token_valid
def get_media_links(request):
    try:
        logger.info("[ GET_MEDIA_LINKS ] User %s requested media links" % request.username)
        g_api = GApi()
        downloadLinks = g_api.getMedia(request.username)
        return JsonResponse({
            "status": 200,
            "msg": "Success",
            "data": {
                "downloadLinks": downloadLinks,
            }
        }, status=200)
    except Exception as e:
        logger.critical("Exception cuaght in file {}".format(__file__), exc_info=True)
        return JsonResponse({
            "status": 400,
            "msg": "failed.",
            "data": {
                "error": str(e),
            }}, status=400)
