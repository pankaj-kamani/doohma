from apps.api import GoogleSpreadsheet, GoogleDrive

class GApi:
    CLIENTK = 'apps/api/ServiceKey.json'
    USERID_COLLECTION = 'Master_Table'
    STREAMING_DOCUMENT = 'Streaming'
    MAPPING_DOCUMENT = 'Mapping'
    SELF_DOCUMENT = 'Self'
    SUCCESS = 0
    FAILED = 1
    KEY = "cHJpbnQgJ2hlbGxvIFdvcmxkICEn"

    def __init__(self):
        self.CLIENTK = "apps/api/ServiceKey.json"
        self.gsClient = GoogleSpreadsheet.authoriseClient(self.CLIENTK)
        self.gdClient = GoogleDrive.authoriseClient(self.CLIENTK)
        self.collection = GoogleSpreadsheet.openGSpreadsheet(self.gsClient, self.USERID_COLLECTION)
        self.doc = GoogleSpreadsheet.selectWorksheetByName(self.collection, self.STREAMING_DOCUMENT)

    def validateUser(self, username, password):
        return self._validateUser(username, password)

    def _validateUser(self, username, password):
        logInStatus = self.FAILED
        try:
            usernameXY = GoogleSpreadsheet.findCell(self.doc, username)
            xPwdServer = GoogleSpreadsheet.getCellValue(self.doc, usernameXY.row, usernameXY.col + 1)
            pwdServer = self.decodePwd(xPwdServer.value, self.KEY)
            if pwdServer == password:
                logInStatus = self.SUCCESS
        except:
            pass
        return logInStatus

    def updateScreenStatus(self, username, isOnline):
        updateStatus = self.FAILED
        try:
            usernameXY = GoogleSpreadsheet.findCell(self.doc, username)
            if isOnline:
                isOnline = "ONLINE"
            else:
                isOnline = "OFFLINE"
            GoogleSpreadsheet.updateCell(self.doc, usernameXY.row, usernameXY.col + 2, isOnline)
            updateStatus = self.SUCCESS
        except:
            pass
        return updateStatus



    def getSelfUsername(self, streamingUsername):
        doc = GoogleSpreadsheet.selectWorksheetByName(self.collection, self.MAPPING_DOCUMENT)
        usernameXY = GoogleSpreadsheet.findCell(doc, streamingUsername)
        selfUser = GoogleSpreadsheet.getCellValue(doc, usernameXY.row, 1)
        return selfUser.value

    def getFolderId(self, streamingUsername):
        selfUsername = self.getSelfUsername(streamingUsername)
        collection = GoogleSpreadsheet.openGSpreadsheet(self.gsClient, self.USERID_COLLECTION)
        doc = GoogleSpreadsheet.selectWorksheetByName(collection, self.SELF_DOCUMENT)
        usernameXY = GoogleSpreadsheet.findCell(doc, selfUsername)
        id = GoogleSpreadsheet.getCellValue(doc, usernameXY.row, usernameXY.col + 2)
        return id.value


    def getMedia(self, username):
        id = self.getFolderId(username)
        downloadLinks = GoogleDrive.getDownloadLinks(id, self.gdClient)
        return downloadLinks


    def decodePwd(self, pwd, key):
        decodedPwd = ""
        for x, y in zip(pwd, key):
            z = ord(x) - ord(y)
            decodedPwd += chr(z)
        return decodedPwd

    def encodePwd(self, pwd, key):
        encodedPwd = ""
        for x, y in zip(pwd, key):
            z = ord(x) + ord(y)
            encodedPwd += chr(z)
        return encodedPwd



#
# USERID_COLLECTION = 'Master_Table'
# MAPPING_DOCUMENT = 'Mapping'
# STATUS_DOCUMENT = "Streaming"
#
# #dynamically populated
# screenStatus = OrderedDict([
#     ("username", []), #streaming app username/s
#     ("status", []),
#     ("access", []),
#     ("lastOnline", [])
# ])
#



# def getScreenStatus(client, username):
#
#     # adding this to fix a bug where multiple calls to this function starts
#     # duplicating the data.
#     global screenStatus
#     screenStatus = OrderedDict([
#         ("username", []),
#         ("status", []),
#         ("access", []),
#         ("lastOnline", [])
#     ])
#
#     collection = GoogleSpreadsheet.openGSpreadsheet(client, USERID_COLLECTION)
#     getSelf2StreamUserMap(collection, MAPPING_DOCUMENT, username)
#     getScreenStats(collection, STATUS_DOCUMENT, screenStatus.get("username"))
#     return screenStatus

# def getSelf2StreamUserMap(collection, docName, username):
#     doc = GoogleSpreadsheet.selectWorksheetByName(collection, docName)
#     try:
#         selfUsernameXY = GoogleSpreadsheet.findCell(doc, username)
#         map = GoogleSpreadsheet.getAllRowValues(doc, selfUsernameXY.row)
#         map.pop(0) #get rid of selfUsername from list
#         screenStatus["username"] = map
#     except:
#         pass

# def getScreenStats(collection, docName, username):
#     doc = GoogleSpreadsheet.selectWorksheetByName(collection, docName)
#     try:
#         for user in username:
#             streamUsernameXY = GoogleSpreadsheet.findCell(doc, user)
#             for i,x in enumerate(screenStatus, start=1):
#                 if x != "username":
#                     stat = GoogleSpreadsheet.getCellValue(doc, streamUsernameXY.row, streamUsernameXY.col + i)
#                     screenStatus[x].append(stat.value)
#     except:
#         pass

