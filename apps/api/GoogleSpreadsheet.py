import gspread
from oauth2client.service_account import ServiceAccountCredentials


def authoriseClient(CLIENTK):
    scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
    creds = ServiceAccountCredentials.from_json_keyfile_name(CLIENTK, scope)
    client = gspread.authorize(creds)
    return client

def openGSpreadsheet(client, spreadSheetName):
    gSheet = client.open(spreadSheetName)
    return gSheet

def selectWorksheetByName(gSheetObj, sheetName):
    sheet = gSheetObj.worksheet(sheetName)
    return sheet

def selectWorksheetByIdx(gSheetObj, wsIndex):
    sheet = gSheetObj.get_worksheet(wsIndex)
    return sheet

def getAllRecords(sheetObj):
    records = sheetObj.get_all_records()
    return records

def getAllValues(sheetObj):
    values = sheetObj.get_all_values()
    return values

def getAllRowValues(sheetObj, rowIndex):
    rwValues = sheetObj.row_values(rowIndex)
    return rwValues

def getAllColumnValues(sheetObj, colIndex):
    colValues = sheetObj.col_values(colIndex)
    return colValues

def getCellValue(sheetObj, rowIndex, colIndex):
    value = sheetObj.cell(rowIndex, colIndex)
    return value

def insertRow(sheetObj, rowIndex, rowValueAsList):
    sheetObj.insert_row(rowValueAsList, rowIndex)

def updateCell(sheetObj, rowIndex, colIndex, newValue):
    sheetObj.update_cell(rowIndex, colIndex, newValue)

def findCell(sheetObj, value):
    cell = sheetObj.find(value)
    return cell

# if __name__ == "__main__":
#     parser = argparse.ArgumentParser(description='CMake build launcher')
#     parser.add_argument('--skip_clean', action='store_true')
#     parser.add_argument('--release', help="--release=False disable build release version")
#     parser.add_argument('--cmake_only', default=False, action='store_true', required=False, help="run only cmake, don't build binaries")
#     parser.add_argument('--use_multi', action='store_true', help="Use GHS MULTI to build")
#     parser.add_argument('--jobs', required=False, type=int, default=8, help="--jobs=8 make job parameter")
#     args = parser.parse_args()
#     main(args)



