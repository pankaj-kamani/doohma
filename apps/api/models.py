from django.db import models

# Create your models here

class Tokens(models.Model):
    username = models.CharField(max_length=255)
    token = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    expiry_time = models.DateTimeField()

    class Meta:
        db_table = 'user_tokens'
        verbose_name = 'User Token'
        verbose_name_plural = 'User Tokens'
