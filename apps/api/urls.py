from django.conf.urls import url
from django.urls import path
from apps.api import views

urlpatterns = [
    path('api-login/', views.api_login, name='api-login'),
    path('is-online/', views.is_online, name='is-online'),
    path('get-media-links/', views.get_media_links, name='is-online'),
]
