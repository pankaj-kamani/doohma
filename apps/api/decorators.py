from functools import wraps

from django.core.exceptions import PermissionDenied
from django.http import JsonResponse

from apps.api.crypt import decrypt_message
from apps.api.models import Tokens


def is_token_valid(function):
    def wrap(request, *args, **kwargs):
        token = request.headers.get('Authorization', None).split(' ')[-1]
        if not token:
            return JsonResponse({"message": 'Credentials not provided'}, status=403)
        try:
            username = decrypt_message(token.encode())
        except Exception as e:
            return JsonResponse({"message": 'Invalid Token'}, status=400)
        try:
            token = Tokens.objects.get(username=username)
            request.username = username
        except:
            return JsonResponse({"message": 'Token Expired !'}, status=400)
        return function(request, *args, **kwargs)
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap
