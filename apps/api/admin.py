from django.contrib import admin

# Register your models here.
from apps.api.models import Tokens

admin.site.register(Tokens)
