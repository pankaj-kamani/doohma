from googleapiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials
from httplib2 import Http
from apiclient import errors

import io


def authoriseClient(CLIENTK):
    scopes = ['https://www.googleapis.com/auth/drive.file']
    credentials = ServiceAccountCredentials.from_json_keyfile_name(CLIENTK, scopes)
    http_auth = credentials.authorize(Http())
    drive = build('drive', 'v3', http=http_auth)
    return drive

def getFilesInFolder(folderId, handle):
    filter = "\'" + folderId + "\'" + " in parents"
    fileIdList = []
    page_token = None
    while True:
        try:
            response = handle.files().list(q=filter,
                                            spaces='drive',
                                            fields='nextPageToken, files(id, name)',
                                            pageToken=page_token).execute()

            for file in response.get('files', []):
                fileIdList.append(file.get('id'))
            page_token = response.get('nextPageToken', None)
            if page_token is None:
                return fileIdList
        except errors.HttpError:
            print("Error getting files Ids from folder!")
            break

def getDownloadLinks(folderId, handle):
    downloadLinks = []
    fileIdList = getFilesInFolder(folderId, handle)
    for file_id in fileIdList:
        handle.permissions().create(body={"role":"reader", "type":"anyone"}, fileId=file_id).execute()
        downloadLinks.append("https://drive.google.com/uc?export=download&id=" + file_id)
    return downloadLinks


