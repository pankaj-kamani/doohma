import json
import base64
import tempfile

import requests
import  logging
from django.conf import settings
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.forms import model_to_dict
from django.http import JsonResponse, HttpResponseRedirect, HttpResponse, Http404
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import View
from django.shortcuts import render, redirect, get_object_or_404
from apps.adverts.constants import AdvertStatus
from apps.adverts.forms import AdvertCreateForm, AdvertVideoForm
from apps.adverts.instagram import Instagram
from apps.adverts.models import Advert, AdvertImages
from django.contrib.auth.decorators import login_required
from apps.payment.models import Subscription


logger = logging.getLogger('main')


def get_as_base64(url):
    with tempfile.NamedTemporaryFile() as tfile:
        tfile.write(requests.get(url).content)
        with open(tfile.name, "rb") as imageFile:
            strng = base64.b64encode(imageFile.read())
        tfile.flush()
    return strng.decode("utf-8")


@login_required
def subscribe(request, pk):
    user = request.user
    try:
        ad_to_pay = Advert.objects.get(user=user, pk=pk)
    except ObjectDoesNotExist:
        return redirect("404")
    request.session['ad_to_pay'] = ad_to_pay.id
    ad_step = request.session.get('ad_step',None)
    if ad_step:
        if ad_step.get('ad_id', None) == pk:
            if ad_step.get('next_step') == 'locations':
                return redirect('locations')
            elif ad_step.get('next_step') == 'sub-plans':
                loc = ad_step.get('loc_selected')
                return redirect(reverse('sub-plans', kwargs={"pk": pk})+'?loc_id={}'.format(loc))
            elif ad_step.get('next_step') == 'confrim_pay':
                return redirect('confrim_pay', package_id=ad_step.get('package_id'))
    return redirect('locations')



@login_required()
def ad_redirect(request):
    try:
        del request.session['ad_to_pay']
    except:
        pass
        logger.info("No ad found in session skiping delete")
    return HttpResponseRedirect('/ad/')

def check_subscription(item):
    subs = item.subscription_set.filter(is_active=True)
    if subs.count():
        return subs.latest('created_ts')
    else:
        return False

@login_required
def adv_list(request):
    user = request.user
    ads = Advert.objects.all().order_by('created_ts')
    ads = ads.filter(user=user).prefetch_related('user')
    page = request.GET.get('page', 1)
    paginator = Paginator(ads, settings.PAGE_SIZE)
    try:
        ads = paginator.page(page)
    except PageNotAnInteger:
        ads = paginator.page(1)
    except EmptyPage:
        ads = paginator.page(paginator.num_pages)

    ad_list = []
    for item in ads:
        item_dict = model_to_dict(item)
        item_dict['currently_susbscribed'] = check_subscription(item)
        item_dict['status'] = item.get_status_display()
        item_dict['created_ts'] = item.created_ts
        ad_list.append(item_dict)
    ad_list = sorted(ad_list, key=lambda x: x['created_ts'], reverse=True)
    return  render(request, 'ad-listing.html', locals())


def get_insta_callback_link(request):
    domain = request.build_absolute_uri('/')[:-1]
    return "{}{}".format(domain, reverse('insta_callback'))


@login_required
def edit_ad(request, ad_id=None):
    try:
        ad = get_object_or_404(Advert, id=ad_id)
        form = AdvertCreateForm(instance=ad)
        # disabled the start_date field on request
        form.fields.get('start_date').disabled = True
        image_set = ad.image_set.all()
        insta_link = "https://api.instagram.com/oauth/authorize?app_id={}&redirect_uri={}&scope=user_profile,user_media&state=1&response_type=code"\
            .format(settings.INSTA_APP_ID, get_insta_callback_link(request))
        return render(request, 'ad-create.html', {
            'ad_id': ad.id,
            'form': form,
            'insta_link': insta_link,
            'image_set': json.dumps([item.image.url for item in image_set]),
            'from_edit': True
        })
    except Exception as e:
        logger.critical("Exception occured %s", str(e))
        raise Http404


@method_decorator(login_required,  name='dispatch')
class AdView(View):
    form_class = AdvertCreateForm
    template_name = 'ad-create.html'
    def get(self, request):
        ad_to_pay = request.session.get('ad_to_pay', None)
        edit = True
        ad_id = None
        if ad_to_pay:
            ad = Advert.objects.get(id=ad_to_pay)
            form = self.form_class(instance=ad)
            ad_id = ad_to_pay
            image_set = json.dumps([item.image.url for item in ad.image_set.all()])
        else:
            form = self.form_class()
            edit = False
            image_set = None
        insta_link = "https://api.instagram.com/oauth/authorize?app_id={}&redirect_uri={}&scope=user_profile,user_media&state=1&response_type=code"\
            .format(settings.INSTA_APP_ID, get_insta_callback_link(request))

        return render(request, self.template_name, {
            'edit': edit,
            'ad_id': ad_id,
            'form': form,
            'insta_link': insta_link,
            'image_set': image_set,
            'dummy_link': reverse('insta_callback')
        })

    def post(self, request):
        redirect_to_list = False
        request_data = request.POST.copy()
        request_data['user'] = request.user.id
        ad_id = request.POST.get('ad_id', None)
        if ad_id:
            request_data.pop('ad_id')
            ad = Advert.objects.get(id=int(ad_id))
            request_data['start_date'] = ad.start_date
            form = self.form_class(request_data, request.FILES, instance=ad)
            request.session['ad_edit'] = {"ad_id": ad.id, 'edit': True}
        else:
            form = self.form_class(request_data)
        if not request.POST.get('images'):
            form.add_error('images', 'Images are required')
        if form.is_bound and form.is_valid():
            if form.instance.pk:
                redirect_to_list = True
                instance = form.instance
                instance.image_set.all().delete()
                if instance.status in [AdvertStatus.Approved, AdvertStatus.Rejected]:
                    instance.status = AdvertStatus.Requested
            else:
                form.instance.status = AdvertStatus.Pending
            instance = form.save()
            instance.save()
            request.session['ad_to_pay'] = instance.id
            request.session['ad_step'] = { "ad_id": instance.id, "next_step": "locations"}
            if redirect_to_list:
                del request.session['ad_edit']
                return HttpResponseRedirect(reverse('adverts_list'))
            return HttpResponseRedirect('/locations/')
        else:
            return render(request, self.template_name, {'form':form, 'ad_id': ad_id })

@login_required
def ad_details(request, ad_id):
    ad = Advert.objects.get(id=ad_id)
    medias = ad.image_set.all()
    sub = check_subscription(ad)
    return render(request, 'ad-details.html', locals())

@login_required
def ad_delete(request, pk):
    try:
        advert = get_object_or_404(Advert, pk=pk)
        x, y = advert.delete()
        messages.add_message(request, messages.ERROR, 'Advert {} deleted '.format(pk))
        return redirect(reverse('adverts_list'))
    except ObjectDoesNotExist:
        messages.add_message(request, messages.ERROR, 'Advert with id{} does not exists'.format(pk))
        return redirect(reverse('adverts_list'))


@login_required
def upload_video(request, pk):
    advert = get_object_or_404(Advert, pk=pk)
    form = AdvertVideoForm(request.POST, request.FILES, instance=advert)
    if form.is_bound and form.is_valid():
        instace = form.save()
        advert.status = AdvertStatus.Approved
        advert.save()
        messages.add_message(request, messages.INFO, "Video Uploaded successfully")
        return HttpResponseRedirect(reverse('campaign-list'))
    return HttpResponseRedirect(reverse('campaign-details',  kwargs={'pk': pk}))


@login_required
def approve_media(request, pk):
    try:
        media = get_object_or_404(AdvertImages, pk=pk)
        media.is_approved=True
        media.save()
        message = "Media Approved successfully"
        return JsonResponse({"message": message, "media_id": pk}, status=200)
    except:
        return JsonResponse({
            "message": "Something went wrong !",
            "media_id": pk,
            "is_last": False
        }, status=500)


@login_required
def reject_media(request, pk):
    try:
        data = json.loads(request.body)
        media = get_object_or_404(AdvertImages, pk=pk)
        advert = media.advert
        media.is_rejected=True
        media.reject_reason = data.get('reject_id')
        media.save()
        advert.is_approved=False
        advert.status = AdvertStatus.Rejected
        message = "Media Approved successfully"
        advert.save()
        return JsonResponse({"message": message, "media_id": pk}, status=200)
    except:
        return JsonResponse({
            "message": "Something went wrong !",
            "media_id": pk
        }, status=500)



@login_required
def broadcast_advert(request, pk):
    try:
        ad = get_object_or_404(Advert, pk=pk)
        status = request.POST.get('status')
        print("status received is=======>  %s" % status)
        status = int(status.strip())
        if status == 5:
            reverse_state = 6
            ad.status = AdvertStatus.BroadcastOn
            message = "Video BroadcastOn Successfully"
        elif status == 6:
            reverse_state = 5
            ad.status = AdvertStatus.BroadcastOff
            message = "Video BroadcastOff successfully"
        else:
            message = "invalid Request"
            reverse_state = -1
        ad.save()
        return JsonResponse({"message": message, "media_id": pk, "status": reverse_state}, status=200)
    except:
        return JsonResponse({
            "message": "Something went wrong !",
            "media_id": pk
        }, status=500)


def insta_callback(request):
    # receive code on successfull authentication by user
    logger.info("Response received from the instagram %s", request.GET)
    try:
        code  = request.GET.get('code')
        inst_obj = Instagram(code=code, call_back_url=get_insta_callback_link(request))
        inst_obj.get_all_media() # populating instagram instance with all medias
        return render(request, 'insta_gallary.html', { 'all_medias': inst_obj.all_media})
        # all_media= [{"id": "17863605664686231", "media_type": "IMAGE", "media_url": "https://scontent.xx.fbcdn.net/v/t51.2885-15/87209398_1755419257916006_3013272381522157338_n.jpg?_nc_cat=102&_nc_ohc=S_BP1-XF88wAX-rbo1z&_nc_ht=scontent.xx&oh=84a074e8568f9fed375bce2d61e47671&oe=5E87396D"}, {"id": "17841700117047165", "media_type": "IMAGE", "media_url": "https://scontent.xx.fbcdn.net/v/t51.2885-15/10954414_786365138107339_59188625_n.jpg?_nc_cat=108&_nc_ohc=zClQi3cBzjgAX-9-EAP&_nc_ht=scontent.xx&oh=e37f7b491d48cbe74a53f8146e4a109a&oe=5E8B4EDE"}] # populating instagram instance with all medias
        # return render(request, 'insta_gallary.html', { 'all_medias': all_media})
    except requests.HTTPError as e:
        e.response.json()
        print(str(e))
        logger.critical("NO code found", exc_info=True)
        return HttpResponse(e.response)

