from django.urls import path
from apps.adverts import views as advert_views
from apps.payment import views

urlpatterns = [
    path("adverts/", advert_views.adv_list, name="adverts_list"),
    path("ad/", advert_views.AdView.as_view(), name="ad-view"),
    path("ad/<int:pk>/upload_video", advert_views.upload_video, name="upload-video"),
    path("ad/create/insta_callback", advert_views.insta_callback, name='insta_callback'),
    path("ad/new/", advert_views.ad_redirect, name="ad-new"),
    path("ad/<int:ad_id>/details", advert_views.ad_details, name="ad-details"),
    path("ad/<int:ad_id>/edit", advert_views.edit_ad, name="ad-edit"),
    path("ad/<int:pk>/subscribe", advert_views.subscribe, name="subscribe"),
    path("ad/<int:pk>/plan", views.subscription_plans, name="sub-plans"),
    path("ad/<int:pk>/delete", advert_views.ad_delete, name="delete-ad"),
    path("ad/media/<int:pk>/approve", advert_views.approve_media, name="approve-media"),
    path("ad/media/<int:pk>/reject/", advert_views.reject_media, name="reject-media"),
    path("ad/broadcast/<int:pk>/", advert_views.broadcast_advert, name="approve-media"),
]
