import datetime
import json
import logging
import os
from django.conf import settings
from django  import forms
from django.core.files.storage import FileSystemStorage
from django.shortcuts import render
from formtools.wizard.views import SessionWizardView
from storages.backends.gcloud import GoogleCloudStorage
from django.core.files.storage import default_storage
from apps.adverts.models import Advert, AdvertImages

import base64
from django.core.files.base import ContentFile
from django.contrib.admin import widgets


logger = logging.getLogger('api')

def base64_file(data, name=None):
    _img_str = data.get('data')
    return ContentFile(base64.b64decode(_img_str), name='{}'.format(name))

class AdvertCreateForm(forms.ModelForm):
    images = forms.FileField(
        widget=forms.ClearableFileInput(attrs={'multiple': True}),
        required=False,
        help_text="Select Multiple images of high quality for best experience")
    start_date = forms.DateField(required=True)

    def clean_start_date(self):
        date = self.cleaned_data['start_date']
        if not self.instance.pk:
            if date < datetime.date.today():
                raise forms.ValidationError("The date cannot be in the past!")
        return date

    class Meta:
        model = Advert
        fields = (
            "ad_title",
            "ad_description",
            "images",
            "user",
            "start_date"
        )

    def __init__(self, *args, **kwargs):
        super(AdvertCreateForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        ad = super(AdvertCreateForm, self).save(commit=False)
        if commit:
            ad.save()
            ad.image_set.all().delete()
            x = dict(self.data.lists())
            img_strs = x.get('images')
            for idx, img in enumerate(img_strs):
                if img:
                    img = json.loads(img)
                    file_name = img.get('name') + str(idx)
                    content_type = img.get('type').split('/')[0]
                    if content_type in ['video', 'application']:
                        media_type=2
                        if len(img.get('name').split('.')) <= 1:
                            file_name += ".mp4"
                            img['type'] = 'video/mp4'
                    else:
                        media_type = 1
                        file_name += ".png"
                    m = GoogleCloudStorage(location=os.path.join('ads/{}'.format(ad.id)))
                    path = m.save(file_name, base64_file(img))
                    tmp_file = os.path.join("/ads/{}".format(ad.id), path)
                    instace = AdvertImages(advert=ad, type=media_type,  image=tmp_file)
                    instace.save()
                    logger.info("image {} created for the advertisement ID {}".format(instace, ad.id))
                else:
                    continue
        return ad

class AdvertImagesForm(forms.ModelForm):
    class Meta:
        model= AdvertImages
        fields= ['image','advert', 'type', 'is_approved']


class AdvertVideoForm(forms.ModelForm):
    video = forms.FileField(required=True)
    class Meta:
        model= Advert
        fields= ['video']