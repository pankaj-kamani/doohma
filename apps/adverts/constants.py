

class AdvertStatus:
    Pending = 0
    Requested = 1
    Approved = 2
    Rejected = 3
    Review = 4
    BroadcastOn = 5
    BroadcastOff = 6

    FieldStr = {
        Pending: "Pending",
        Requested : "Requested",
        Approved: "Approved",
        Rejected: "Rejected",
        Review: "Review",
        BroadcastOn: "Broadcast On",
        BroadcastOff: "Broadcast Off"
    }

    @classmethod
    def get_choices(cls):
        return cls.FieldStr.items()


class MediaType:
    Image = 1
    Video = 2


    FieldStr = {
        Image : "Image",
        Video: "Video",

    }

    @classmethod
    def get_choices(cls):
        return cls.FieldStr.items()


class RejectOptions:

    BAD_QUALITY = 1
    option2 = 2
    option3 = 3
    option4 = 4


    FieldStr = {
        BAD_QUALITY : "Bad Quality",
        option2 : "option1",
        option3 : "option1",
        option4 : "option1",

    }

    @classmethod
    def get_choices(cls):
        return cls.FieldStr.items()