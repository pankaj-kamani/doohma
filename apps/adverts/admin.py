from django.contrib import admin
from apps.adverts.models import  *
# Register your models here
from apps.common.base_admin import BaseAdmin


class AdvertAdmin(BaseAdmin):
    list_display = ['user', 'ad_title', 'ad_description', 'status', 'is_paid']
    readonly_fields = ['user',]

class AdvertImagesAdmin(BaseAdmin):
    list_display = ['advert', 'image', 'type']



admin.site.register(Advert, AdvertAdmin)
admin.site.register(AdvertImages, AdvertImagesAdmin)