# Generated by Django 2.0 on 2019-12-15 07:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('adverts', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='advert',
            name='is_paid',
            field=models.BooleanField(default=False),
        ),
    ]
