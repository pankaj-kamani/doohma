import os
from datetime import timedelta

from django.contrib.auth import get_user_model
from django.db import models
from apps.adverts.constants import AdvertStatus, MediaType, RejectOptions
from apps.common.base_model import BaseModel
from .validators import validate_file_extension

User = get_user_model()

def get_advert_image_path(instance, filename):
    return os.path.join("advertisements/%s/%s" % (instance.id, filename))


class Advert(BaseModel):
    ad_title = models.CharField(max_length=255)
    ad_description = models.TextField()
    status = models.SmallIntegerField(choices=AdvertStatus.get_choices(), default=AdvertStatus.Pending)
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)
    is_paid = models.BooleanField(default=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    is_approved = models.BooleanField(default=False)
    video = models.FileField(
        "Uploaded Video",
        upload_to=get_advert_image_path,
        blank=True,
        null=True,
        validators=[validate_file_extension]
    )

    def __str__(self):
        return "{}".format(self.ad_title)

    class Meta:
        db_table = "dh_advert"
        verbose_name_plural = "Adverts"

    @property
    def video_url(self):
        if self.video and hasattr(self.video, 'url'):
            return self.video.url
        return None

    def save(self, *args, **kwargs):
        if not self.pk:
            self.end_date = self.start_date + timedelta(days=30)
        return super(Advert, self).save(*args, **kwargs)

class AdvertImages(BaseModel):
    image = models.FileField(upload_to=get_advert_image_path)
    advert = models.ForeignKey(Advert, on_delete=models.CASCADE, related_name="image_set")
    type = models.SmallIntegerField(choices=MediaType.get_choices(), default=MediaType.Image)
    is_approved = models.BooleanField(default=False)
    is_rejected = models.BooleanField(default=False)
    is_broadcast = models.BooleanField(default=False)
    reject_reason = models.IntegerField(choices=RejectOptions.get_choices(), default=RejectOptions.BAD_QUALITY)

    def __str__(self):
        return "{}".format(self.advert)

    class Meta:
        db_table = 'dh_advert_media'
        verbose_name_plural = "Adverts Medias"