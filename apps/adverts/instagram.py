import logging


import requests
from django.conf import settings


logger = logging.getLogger('main')

class Instagram(object):

    CLIENT_ID = settings.INSTA_APP_ID
    CLIENT_SECRET = settings.INSTA_APP_SECRET

    def __init__(self, code=None, call_back_url=None):

        logger.info('Initializing the instagram object with code %s', code)
        self.code = code
        self.access_token = None
        self.user = None
        self.all_media = []
        self.graph_endpoint = "https://graph.instagram.com/"
        self.base_url = "https://api.instagram.com/"
        self.access_token_endpoint = "oauth/access_token"
        self.media_id_endpoint = "me/media"
        self.call_back_url = call_back_url
        self.get_access_token()


    def get_access_token(self):
        assert self.code != None, "Init the Instagram object first"
        payload = {
            'client_id': self.CLIENT_ID,
            'client_secret': self.CLIENT_SECRET,
            'grant_type': 'authorization_code',
            'redirect_uri': self.call_back_url,
            'code': self.code}
        url = self.base_url + self.access_token_endpoint
        response = requests.post(url, data=payload)
        if response.status_code == 200:
            data_dict = response.json()
            token, user = data_dict.get('access_token'), data_dict.get('user')
            # get access token from instagram and init it to use on later apis
            self.user = user
            self.access_token = token
            logger.info('Access token received is %s for user %s', (token, user))
        else:
            response.raise_for_status()

    def get_user_media(self):
        params = {
            'access_token': self.access_token,
            'fields': 'id'
        }
        url = self.graph_endpoint + self.media_id_endpoint
        response = requests.get(url, params=params)
        response.raise_for_status()
        return response.json()

    def get_media_node_url(self, node_id):
        return "{0}/{1}".format(self.graph_endpoint, node_id)

    def get_media_node(self, node_id):
        url = self.get_media_node_url(node_id)
        params = {
            'fields': "id,media_type,media_url",
            'access_token': self.access_token
        }
        node = requests.get(url, params=params)
        return node.json()

    def get_all_media(self):
        user_medias = self.get_user_media()
        for media in user_medias.get('data'):
            node = self.get_media_node(media['id'])
            self.all_media.append(node)
            logger.info("Node Received is ===> \n %s", node)